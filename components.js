import React, {Component} from 'react';
import {PropTypes} from 'prop-types';
export class BeerListContainer extends Component {
  constructor(props) {
    super(props);
    this.state = {
      beers: []
    }
    this.addItem = this.addItem.bind(this);
  }
  addItem(name : string) {
    this.setState({
      beers: [].concat(this.state.beers).concat([name])
    });
  }

  render() {
    return (
      <div>
        <InputArea onSubmit={this.addItem}/>
        <BeerList/>
      </div>
    )
  }

}

// BeerListContainer.propTypes = {
//   onSubmit: PropTypes.func.isRequired
// }

export class InputArea extends Component {
  constructor(props) {
    super(props);
    this.state = {
      value:''
    }
    this.setText = this.setText.bind(this);
  }
  setText(event) {
    this.setState({value: event.target.value});
  }

  render() {
    return (
      <div>
        <input type="text" value={this.state.value} onChange={this.setText} />
      <button>Add</button>
      </div>
    )
  }
}

export class BeerList extends Component {
  render() {
    return <ul/>
  }
}
